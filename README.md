# Zombie Runner V1.0

FPS zombie game

## Getting started

V1.0 of my Zombie Runner project from a GameDev.TV course. Plans are to add music, SFX, player health display, mission items display (x of 4), and tweak the zombie animations. Thank you for playing my very first game. :-)

